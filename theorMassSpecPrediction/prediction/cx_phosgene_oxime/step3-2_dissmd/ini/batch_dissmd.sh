#!/bin/bash
 
 
#SBATCH --partition=allcpu,maxcpu # this is the default partition.
#SBATCH --time=25:00:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.
#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like
#SBATCH --job-name  DissMD!
#SBATCH --output     task.out  # File to which STDOUT will be written
#SBATCH --error      task.err  # File to which STDERR will be written
#SBATCH --mail-type ALL                 # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user denis.tikhonov@desy.de  # Email to which notifications will be sent
 
export LD_PRELOAD=""
source /etc/profile.d/modules.sh
 
 
 
#### SETUP THE SIMULATIONS PARAMETERS ####################
# Number of processes per task
PrPerTask=4
# Number of jobs to run
NumOfJobs=50
# Input file with the molecule
InpXYZ="ini.xyz"
# File, where the pulses are defined
PulseDef="pulse.def"
# Total simulation time (after pump/probe pulses)
SimTime="10.0"
# IP stretching (see the manual)
IPStretching="0.4"
# Dissociation period
DissPeriod="20.0"
# Pump-probe delay, in ps
PPDel="0.0"
# Cross-correlation time, in fs
TCC="0.0"
# Threshold for the bonds detection
BondsScale="2."
#### END OF SETUP THE SIMULATIONS PARAMETERS #############
 
# save the path to the initial directory
IniDir=$(pwd)
 
# Settings from xtb -h, trying to parallelize XTB
export MKL_NUM_THREADS=$PrPerTask
export OMP_NUM_THREADS=$PrPerTask
export OMP_STACKSIZE=4G
 
#path to the PyRAMD code
#PATH2Script="/gpfs/petra3/scratch/choprapr/400-800/05072021/PyRAMD"
PATH2Script="/home/tikhonov/pyramd"
#
 
echo "Heeelllllooooo" $(nproc)
aprocs=$(nproc --all)
echo "Total number of processors on $HOSTNAME is $aprocs "
procs=$(( $aprocs / 2 ))
echo " Number of phyiscal cores on $HOSTNAME $procs "
 
# total number of task for simulation
NumSimTasks=$(echo $procs/$PrPerTask | bc)
echo " It was decided to use $PrPerTask processers per 1 job (i.e. $NumSimTasks will be run at the same time) "
 
IniN=$(ls ../ | grep 'jobs[0-9]\+' | sed 's/jobs//g' | sort -n | tail -1)
if [ -z "$IniN" ]; then
IniN=0
fi
 
 
# start all the jobs iteratively
for ((n=$IniN; n<$NumOfJobs; n++)); do
    # change to the initial directory
    cd $IniDir
    # get the number of active jobs
    NumActPr=$(jobs -l | grep -c 'dissMD')
    # compare the number running processes (NumActPr) with the maximal number of the running jobs (NumSimTasks)
    DName=$(printf "job%05i" $n) # this is the name of the new directory
    # iterate through this cycle, while the directory for the simulation is not created
    while [ ! -d "$DName" ]; do
        NumActPr=$(jobs -l | grep -c 'dissMD')
        if [ "$NumActPr" -ge "$NumSimTasks" ]
            then # if there are no free job slots, wait
                sleep 5s
            else # if there is a chance, start the job
 
                mkdir $DName # create the directory, where the dynamics will be run
                cd $DName    # go to the directory, where the dynamics will be run
             
                ###################### This is the actual command to run the job ########################
                $PATH2Script/dissMD.py  --inifile ../$InpXYZ  --PulsesDef ../pulse.def ../$PulseDef --SimTime $SimTime  --DissPeriod $DissPeriod --PPDel $PPDel --CCTime $TCC --BondsScale $BondsScale  > dmd.log &
                #########################################################################################
             
                cd ../ # go back
        fi
    done
done
 
NumActPr=$(jobs -l | grep -c 'dissMD')
while [ "$NumActPr" -gt 0 ]
    do
        sleep 10s
        NumActPr=$(jobs -l | grep -c 'dissMD')
        echo $NumActPr " left "
        jobs -l
    done

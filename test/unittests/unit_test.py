import sys
sys.path.append('../../src/toxic_ms')

import unittest
import numpy as np
import toxicMS as tms

# take examples from, e.g.:
#      https://stash.desy.de/projects/PYRAMD/repos/pyramd/browse/tests/test_modules/test_reading_of_the_data.py

class TestVariablesReading(unittest.TestCase):

    def test_jdx_conversion(self):
        
        Ref = np.array([[12,420], [13,170], [14,170], [15,10],
                        [26,1682], [27,9999], [28,170], [29,10]],dtype=float)
                         
        Ref[:,1] *= 100. /np.sum(Ref[:,1])

        tms.ConvertJDXToDatFile("74-90-8-Mass.jdx")
        data = np.loadtxt("ref.ms")        
        #print(data)
        #print(Ref)
        res = np.allclose(data, Ref, rtol=1e-02, atol=1e-03, equal_nan=False)
        
        self.assertTrue(res)


    def test_ms_creation(self):
        
        ms = np.array([[5,1],[20,100]])
        #print(ms)
        x0 = 0
        x1 = 50
        X = np.linspace(x0,x1,1000)
        W = 0.5
        S = 0.9
        Y = ms[0][1] * np.exp(-0.5*(X-ms[0][0])**2/W**2) + ms[1][1] * np.exp(-0.5*(X-ms[1][0])**2/W**2)
        Y *= S
        Xtest,Ytest = tms.GenerateSpectrum(MS=ms, xini=x0, xfin=x1, npts=len(X), scale=S, width=W)
        
        res = True
        #print(X.shape,Y.shape,Xtest.shape,Ytest.shape)
        #print(np.min(Y-Ytest))
        #print(np.sum(np.abs(Y-Ytest)))
        res *= np.allclose(X, Xtest, rtol=1e-02, atol=1e-03, equal_nan=False)
        res *= np.allclose(Y, Ytest, rtol=1e-02, atol=1e-03, equal_nan=False)
        
        self.assertTrue(res)

    def test_spectra_comparison_good(self):
        ms = np.array([[12,420], [13,170], [14,170], [15,10],
                        [26,1682], [27,9999], [28,170], [29,10]],dtype=float)
                         
        ms[:,1] *= 100. /np.sum(ms[:,1])
        x0 = 0
        x1 = 50
        W = 0.5
        S = 0.9
        X,Y = tms.GenerateSpectrum(MS=ms, xini=x0, xfin=x1, npts=1000, scale=S, width=W)
        
        Npeaks, N, dkl, dbh, dhe = tms.CompareSpectrumWithRef(X,Y, ms, width=1.0)
        #print(Npeaks, N, dkl, dbh, dhe)
        self.assertTrue(Npeaks == N)

    def test_spectra_comparison_bad(self):
        ms = np.array([[12,420], [13,170], [14,170], [15,10],
                        [26,1682], [27,9999], [28,170], [29,10]],dtype=float)
                         
        ms[:,1] *= 100. /np.sum(ms[:,1])
        
        msgen = np.array([[5,1],[20,100]])
        x0 = 0
        x1 = 50
        W = 0.5
        S = 0.9
        X,Y = tms.GenerateSpectrum(MS=msgen, xini=x0, xfin=x1, npts=1000, scale=S, width=W)
        
        Npeaks, N, dkl, dbh, dhe = tms.CompareSpectrumWithRef(X,Y, ms, width=1.0)
        #print(Npeaks, N, dkl, dbh, dhe)
        self.assertTrue(Npeaks == 0)

    def test_spectra_similarity_matrixes(self):
        Spec = []
        Spec.append(np.loadtxt("data4testing/h.ms"))
        Spec.append(np.loadtxt("data4testing/q.ms"))       
        Spec.append(np.loadtxt("data4testing/t.ms"))    
                   
        Spec.append(np.loadtxt("data4testing/ga.ms"))                       
        Spec.append(np.loadtxt("data4testing/gb.ms"))  
        Spec.append(np.loadtxt("data4testing/vx.ms"))                       
        Spec.append(np.loadtxt("data4testing/ve.ms"))          
        
        NSpec = len(Spec)
        
        DKL = np.zeros( (NSpec,NSpec) , dtype=float )
        DBH = np.zeros( (NSpec,NSpec) , dtype=float )
        DHE = np.zeros( (NSpec,NSpec) , dtype=float )        
        NP  = np.zeros( (NSpec,NSpec) , dtype=float )
        
                
        for i in range(0,NSpec):
            for j in range(0,NSpec):
                X,Y = tms.GenerateSpectrum(MS=Spec[i], xini=0, xfin=300, npts=1000, scale=1, width=0.5)
                Y = tms.CleanSpectrum(Y, NumIter=3, Scale = 3)
                Npeaks, N, dkl, dbh, dhe = tms.CompareSpectrumWithRef(X,Y, Spec[j], width=1.0)
                DKL[i][j] = dkl
                DBH[i][j] = dbh
                DHE[i][j] = dhe
                NP[i][j]  = 100*Npeaks/N
                
        np.savetxt("data4plotting/dkl.mt", DKL)                             
        np.savetxt("data4plotting/dbh.mt", DBH)                             
        np.savetxt("data4plotting/dhe.mt", DHE)        
        np.savetxt("data4plotting/np.mt", NP)                                                                          

    def test_loading_database(self):
        db = tms.LoadDataBase(DataBaseFolder="testDataBase")
        self.assertTrue(len(db) == 2)
        
    def test_analysis(self):
        bkl,bbh,bhe = tms.RunAnalysis(FileToAnalyze="h_sim.dat", DataBaseFolder="testDataBase",
                                      PerformSpectrumCleaning=True, NumIter=3, Scale=3,
                                      Width=1.0)
        exp = "test1/C4H8Cl2S_mustard_gas"
        self.assertTrue( (bkl == exp) * (bbh == exp) * (bhe == exp) )
        
    def test_creating_coarse_ms(self):
        data = np.loadtxt("only_peaks.dat")
        CoarseMS = tms.CreateCoarseMSFromPeaks(data)
        ExpectedArray = np.array([[0,0.],
                                  [1,2.],
                                  [2,2.],
                                  [3,0.]])
        
        res = np.allclose(CoarseMS, ExpectedArray, rtol=1e-02, atol=1e-03, equal_nan=False)                                      
        self.assertTrue(res)
        
    def test_analysis_peaks_only(self):
        bkl,bbh,bhe = tms.RunAnalysis(FileToAnalyze="h_peaks.dat", DataBaseFolder="testDataBase",
                                      PerformSpectrumCleaning=True, NumIter=3, Scale=3,
                                      Width=1.0, OnlyPeaks=True)
        exp = "test1/C4H8Cl2S_mustard_gas"
        self.assertTrue( (bkl == exp) * (bbh == exp) * (bhe == exp) )        

    def test_analysis_peaks_only_forgot_to_extrapolate(self):
        bkl,bbh,bhe = tms.RunAnalysis(FileToAnalyze="h_peaks.dat", DataBaseFolder="testDataBase",
                                      PerformSpectrumCleaning=True, NumIter=3, Scale=3,
                                      Width=1.0, OnlyPeaks=False)
        exp = "test1/C4H8Cl2S_mustard_gas"
        self.assertTrue( (bkl == exp) * (bbh == exp) * (bhe == exp) )        


                
if __name__ == '__main__':
    unittest.main()
    
    
    

import numpy as np

data = np.loadtxt("ref_exp.ms")

ExpPos = set(np.array(data[:,0], dtype=int))

data = np.loadtxt("ref.ms")

TheorPos = set(np.array(data[:,0], dtype=int))

Inter = ExpPos.intersection(TheorPos)
print("Shared %i peaks of %i experimental (%.1f %%):" % (len(Inter),len(ExpPos),100.*len(Inter)/len(ExpPos)))
print(np.sort(list(Inter)))

Missing = ExpPos.difference(TheorPos) 
print("Missing peaks are at m/z:")
print(np.sort(list(Missing)))


data = np.loadtxt("ref_qcxms.ms")
TheorPos = set(np.array(data[:,0], dtype=int))
Inter = ExpPos.intersection(TheorPos)
print("QCxMS: shared %i peaks of %i experimental (%.1f %%):" % (len(Inter),len(ExpPos),100.*len(Inter)/len(ExpPos)))
print(np.sort(list(Inter)))


data = np.loadtxt("ref_dissmd.ms")
TheorPos = set(np.array(data[:,0], dtype=int))
Inter = ExpPos.intersection(TheorPos)
print("DissMD: shared %i peaks of %i experimental (%.1f %%):" % (len(Inter),len(ExpPos),100.*len(Inter)/len(ExpPos)))
print(np.sort(list(Inter)))

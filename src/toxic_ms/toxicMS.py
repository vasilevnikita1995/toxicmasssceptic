#! /usr/bin/python

######################################################################
#                                                                    #
# This script should help with assigning multicomponent mass-spectra #
#                                                                    #
######################################################################

import numpy as np
import argparse
import os

# this function converts any given *jdx file with mass-spec from NIST
# into a "ref.ms" file, containing reference MS in two-column format
def ConvertJDXToDatFile(JDXFileName):
    try:
        # open file and read the peaks
        print("Reading JDX MS file for conversion: %s" % (JDXFileName))
        inpf = open(JDXFileName, "r")
        Read=False
        peaks = []
        for line in inpf:
            if line[0]!="#":
                if Read:
                    words = line.split()
                    for word in words:
                        vals = word.split(",")
                        peaks.append((int(vals[0]),int(vals[1])))
            else:
                if Read and line[0:4]=="##END":
                    Read = False
                else:
                    words = line.split()
                    if words[0] == '##PEAK':
                        Read = True
        inpf.close()
        print("File was successfully read, found %i peaks" % (len(peaks)))
        # find maximal intensity, for renormalization for 100
        Ysum = np.sum([a[1] for a in peaks])
        outf = open("ref.ms", "w")
        outf.write("#%7s %10s\n" % ("m/z", "Intensity") )
        for peak in peaks:
            x = peak[0]
            y = 100. * peak[1]/Ysum
            outf.write(" %7i %10.4f\n" % (x,y))
        outf.close()
        return True
        
    except:
        print("Could not read JDX file %s" % (JDXFileName))
        return False


# converts a stick spectrum into a smooth line
# MS is a (Npts,2)-shaped array with MS[:,0] being the peak m/z values, and MS[:,1] being peak intensities
# xini/xfin are initial and final values of the spectra
# width is the standard deviation of the Gaussian, that is used for each peak
# scale is the overall scale of the spectrum
# npts is the number of points
# RETURNS:
#         X,Y -- two 1D arrays of length npts, with X being m/z axis and Y being intensity axis
def GenerateSpectrum(MS, xini=0, xfin=None, npts=1000, scale=1.0, width=0.3):
    if xfin is None:
        xfin = max(MS[:,0])+20*width
    x = np.linspace(xini,xfin,npts)
    y = np.zeros(x.shape,dtype=float)
    for peak in MS:
        y += peak[1] * np.exp( -0.5*(x-peak[0])**2/width**2)
    y *= scale
    return x,y    


# integration of the m/z MS
def GetPeakIntensity(mz, X, Y, width):
    mask = np.where( np.abs(X - mz) <= 0.5*width, True, False)
    val = np.sum(Y[mask])
    return val

# Kullback–Leibler divergence calculation
def DKL(Y, Yref):
    res = 0.
    for i,y in enumerate(Y):
        if y>1.0e-15:
            res += y * np.log(y/Yref[i])
    return res

# Bhattacharyya distance calculation
# https://en.wikipedia.org/wiki/Bhattacharyya_distance
def DBh(Y, Yref):
    BC = np.sum(np.sqrt(Y*Yref))
    BC /= 100. # this is because the Y and Yref are normed by 100 in total
    #print(BC)
    res = - np.log(BC)
    return res

# Hellinger distance calculation
# https://en.wikipedia.org/wiki/Hellinger_distance
def DHe(Y, Yref):
    BC = np.sum(np.sqrt(Y*Yref))
    BC /= 100. # this is because the Y and Yref are normed by 100 in total
    res = np.sqrt(1.0 - BC)
    return res


# this function returns the number of found lines
def NumOfLines(Y):
    N = len(Y)
    mask = np.where(Y>1.0e-15, 1, 0)
    Npeaks = np.sum(mask)
    return Npeaks,N    


# This function compares the continous mass-spectrum with a peak-positioned reference
# Takes: X,Y two 1D arrays of equal length, with X being m/z axis and Y being intensity axis
#        MS is a (Npts,2)-shaped array with MS[:,0] being the peak m/z values, and MS[:,1] being peak intensities
# Returns:
#          *EVERYTHING*
def CompareSpectrumWithRef(X,Y, MS, width=1.0):
    Ytest = np.array([GetPeakIntensity(peak[0], X, Y, width) for peak in MS])
    SumY = np.sum(Ytest)    
    Yref = MS[:,1]
    Npeaks, N = NumOfLines(Ytest)
    
    if Npeaks > 0:   
        Ytest *= 100./SumY
        #print(Ytest,Yref)
        dkl = DKL(Ytest, Yref)
        dbh = DBh(Ytest, Yref)
        dhe = DHe(Ytest, Yref)
        
        return Npeaks, N, dkl, dbh, dhe
    else:
        return Npeaks, N, np.inf, np.inf, np.inf

# this function sets the baseline to zero (assuming that most of the spectrum is empty)
# takes: Y, intensities, returns the same array of cleaned intensities
def CleanSpectrum(Y, NumIter=3, Scale = 3, Silent=True):
    if not Silent:
        print("Initializing spectral cleaning with %i iterations in the std * %f range" % (NumIter, Scale))
    B = np.array(Y)
    for n in range(0,NumIter):
        std = np.std(B)
        mask = np.where(Y > std * Scale * 0.5, False, True)
        B = Y[mask]
    Res = np.where(Y > std * Scale * 0.5, Y, 0.0)
    return Res    

# load database from a folder with path given in str DataBaseFolder
def LoadDataBase(DataBaseFolder="database"):
    print("#######################################################")
    print(" * Loading database from directory %s" % (DataBaseFolder))
    
    database = dict()
    for d in os.listdir(DataBaseFolder):
        try:
            print(" * * Loading from %s subfolder" % (d))
            for dd in os.listdir(DataBaseFolder+"/"+d):
                print(" * * * Loading "+dd)
                try:
                    data = np.loadtxt(DataBaseFolder+"/"+d+"/"+dd+"/ref.ms")
                    database.update({d+"/"+dd: data})  
                except:
                    pass
        except:
            pass
    print("Loaded %i species" % (len(database)))
    return database

# this function takes the peaks position and creates a coarse version of the MS
# to enable analysis of the pre-cleaned and pre-assigned MS
def CreateCoarseMSFromPeaks(data):
    MaxMass = int(np.max(data[:,0]))+2 # this is the maximal m/z in the MS
    x = np.arange(0,MaxMass,1) # these is the x-axis of the spectrum
    y = np.zeros(x.shape, dtype=float) # here, the intensities will be given
    for peak in data: # iterate through the peaks
        ind = int(np.round(peak[0])) # round the m/z value to nearest integer
        y[ind] += peak[1] 
    return np.stack([x,y],axis=-1)


# this function runs an analysis for a file "FileToAnalyze" (str)
# with database from (str) DataBaseFolder folder
def RunAnalysis(FileToAnalyze, DataBaseFolder="database",
                PerformSpectrumCleaning=False, NumIter=3, Scale=3,
                Width=1.0, OnlyPeaks=False):
    print("Starting analysis for a file "+FileToAnalyze)
    DB = LoadDataBase(DataBaseFolder)

    try:
        data = np.loadtxt(FileToAnalyze)
        if OnlyPeaks: # if only the peaks are present, fill everything with zeros, except for the integer peaks positions with step of 1
            data = CreateCoarseMSFromPeaks(data)
        else:
            if PerformSpectrumCleaning:
                data[:,1] = CleanSpectrum(data[:,1], NumIter=NumIter, Scale = Scale, Silent=False)
    except:
        raise RuntimeError("ERROR! Could not analyze the file "+str(FileToAnalyze))
    
    # here we will store the scoring results
    DKL = dict()
    DBh = dict()
    DHe = dict()
    NP  = dict()
    
    # here, we store the best scored result for a given set ouf data
    bestKL = None
    bestBh = None
    bestHe = None
    
    # here, we dump the scores
    outf = open(FileToAnalyze+"_scoring.res", "w")
    outf.write("#%10s %10s %10s %7s %7s  %s\n" % ("DKL", "DBh", "DHe", "N(obs)", "N(tot)", "Substance" ))
    for key in DB.keys():
        print("## Testing "+key+" now, scores:", flush=True)
        Npeaks, N, dkl, dbh, dhe = CompareSpectrumWithRef(data[:,0],data[:,1], DB[key], width=Width)
        outf.write(" %10.5f %10.5f %10.5f %7s %7s %s\n" % (dkl, dbh, dhe, Npeaks, N, key) )
        outf.flush()
        print("    %i out of %i peaks found (%.4f %%)" % (Npeaks, N, 100. * Npeaks/N))
        print("    K-L divergence DKL = %15.10f %%" % (dkl))
        print("    Bhattacharyya distance DBh = %15.10f " % (dbh))
        print("    Hellinger distance DHe = %15.10f " % (dhe))  
        
        DKL.update({key: dkl})
        DBh.update({key: dbh})
        DHe.update({key: dhe})
        NP.update({key: (Npeaks, N)})
        
        if (bestKL is None) or (DKL[bestKL] > dkl):
            bestKL = key
        
        if (bestBh is None) or (DBh[bestBh] > dbh):
            bestBh = key       

        if (bestHe is None) or (DHe[bestHe] > dhe):
            bestHe = key   
      
    outf.close()
    
    print("#################################################################")
    print("#   Results of the analysis : ###################################")
    print("#################################################################\n\n")
    
    print(" * According to Kullback-Leibler Divergence (DKL) : "+bestKL)
    print("     %i out of %i peaks found (%.4f %%)" % (NP[bestKL][0], NP[bestKL][1], 100. * NP[bestKL][0]/NP[bestKL][1]))
    print("     K-L divergence DKL = %15.10f %%" % (DKL[bestKL]))
    print("     Bhattacharyya distance DBh = %15.10f " % (DBh[bestKL]))
    print("     Hellinger distance DHe = %15.10f \n\n" % (DHe[bestKL]))      

    print(" * According to Bhattacharyya Distance (DBh) : "+bestBh)
    print("     %i out of %i peaks found (%.4f %%)" % (NP[bestBh][0], NP[bestBh][1], 100. * NP[bestBh][0]/NP[bestBh][1]))
    print("     K-L divergence DKL = %15.10f %%" % (DKL[bestBh]))
    print("     Bhattacharyya distance DBh = %15.10f " % (DBh[bestBh]))
    print("     Hellinger distance DHe = %15.10f \n\n" % (DHe[bestBh]))  

    print(" * According to Hellinger Distance (DHe) : "+bestHe)
    print("     %i out of %i peaks found (%.4f %%)" % (NP[bestHe][0], NP[bestHe][1], 100. * NP[bestHe][0]/NP[bestHe][1]))
    print("     K-L divergence DKL = %15.10f %%" % (DKL[bestHe]))
    print("     Bhattacharyya distance DBh = %15.10f " % (DBh[bestHe]))
    print("     Hellinger distance DHe = %15.10f \n\n" % (DHe[bestHe]))  
    
    return bestKL, bestBh, bestHe    
        
#! /bin/bash
  
  
NTot=$(ls set* | grep 'job[0-9]\+' | wc -l)
NEnd=$(ls set*/* | grep -c "summary_dissociation_md.log")
printf " %i jobs found ; %i successfully done (%5.2f %%)\n\n" $NTot $NEnd $(echo "100 * $NEnd/$NTot" | bc -l) > ms_process.log
  
cat */*/intermediate_dissociation_md.log | grep 'm.z' | sort -n -k 16 > md.log



python3 <<-EOF
import numpy as np
 
ions = np.loadtxt("md.log", usecols=15)
maxIon=int(max(ions))+1
 
h,be = np.histogram(ions,bins=maxIon+1,range=(0.,maxIon),density=True)
x = 0.5*(be[:-1] + be[1:])
np.savetxt("ms.dat", np.stack([x,h],axis=-1),  fmt='%15.10f')


IntIonMZ = np.array(np.round(ions), dtype=int)

UniqMZ = np.sort( list(set(IntIonMZ)) )
UniqI = np.zeros(UniqMZ.shape, dtype=float)
for n,mz in enumerate(UniqMZ):
    mask = np.where(IntIonMZ == mz, True, False)
    UniqI[n] = len(mask[mask])

UniqI *= 100./np.sum(UniqI)

np.savetxt("ref.ms", np.stack([UniqMZ,UniqI], axis=-1) )

EOF


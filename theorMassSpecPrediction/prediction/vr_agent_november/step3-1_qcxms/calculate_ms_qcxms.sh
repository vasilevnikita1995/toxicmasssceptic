#! /bin/bash

getres > ms_spec.log
plotms >> ms_spec.log

python <<-EOF

import numpy as np

data = np.loadtxt("result.jdx")

X = data[:,0]
Y = data[:,1]

IntIonMZ = np.array(np.round(X), dtype=int)

UniqMZ = np.sort( list(set(IntIonMZ)) )
UniqI = np.zeros(UniqMZ.shape, dtype=float)
for n,mz in enumerate(UniqMZ):
    mask = np.where(IntIonMZ == mz, True, False)
    UniqI[n] = np.sum(Y[mask])

UniqI *= 100./np.sum(UniqI)

np.savetxt("ref.ms", np.stack([UniqMZ,UniqI], axis=-1) )

EOF



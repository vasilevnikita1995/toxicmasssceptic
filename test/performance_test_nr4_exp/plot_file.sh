#! /bin/bash

if echo $1 | grep -q 'xls$'; then
    echo "Processed spectrum $1"
################################################################
#   Plotting processed spectrum

python <<-EOF
import xlrd
import numpy as np

WorkBook = xlrd.open_workbook(filename="$1")
sh = WorkBook.sheet_by_index(0)
iCurrent = 3 # starting index
FailedToRead = False

Data=[]

while not FailedToRead:
    try:
        x = float(sh.cell_value(rowx=iCurrent, colx=0))
        y = float(sh.cell_value(rowx=iCurrent, colx=1))
        Data.append(np.array([x,y]))
        iCurrent += 1
    except:
        FailedToRead = True
Data = np.array(Data)
Data[:,1] *= 100./np.max(Data[:,1])
np.savetxt("$1-tmp.spec", Data)

EOF

echo """
set xlabel 'm/z'
set ylabel 'Intensity, arb. units'

set style fill transparent solid 0.5 # noborder

plot '$1-tmp.spec' u 1:2:(0.2) w boxes

pause -1
""" > tmp.gnp

gnuplot tmp.gnp



################################################################    
elif echo $1 | grep -q 'raw\.csv$'; then
    echo "Raw file $1"
################################################################
#   Plotting raw spectrum   

python <<-EOF
import numpy as np

inpf = open("$1", "r")

count=0
for line in inpf:
    word = line.split(",")
    if count>0:
        try:
            a = np.array(word[1:],dtype=float)
            if count == 1:
                res = [ a ]
            else:
                if a.shape[0] == res[0].shape[0]:
                    res.append(a)
        except:
            break
    count += 1
inpf.close()

res = np.array(res).T
np.savetxt("$1-tmp.spec", res)
EOF

echo """
set xlabel 'm/z'
set ylabel 'Intensity, arb. units'

set style fill transparent solid 0.5 # noborder

set title '$1'

plot for [i=2:100] '$1-tmp.spec' u 1:i w l notitle

pause -1
""" > tmp.gnp

gnuplot tmp.gnp

################################################################    
else
    echo "Unknown file $1"
fi

rm -f */*tmp.spec tmp.gnp







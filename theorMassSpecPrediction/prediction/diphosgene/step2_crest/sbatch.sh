#! /bin/bash

#SBATCH --partition=maxcpu    # this is the default partition.
#SBATCH --time=7:00:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.
#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like 
#SBATCH --job-name  crest
#SBATCH --output    job.log  # File to which STDOUT will be written
#SBATCH --error     job.err  # File to which STDERR will be written
#SBATCH --mail-type ALL                 # Type of email notification- BEGIN,END,FAIL,ALL


export MKL_NUM_THREADS=10
export OMP_NUM_THREADS=10
export OMP_STACKSIZE=4G

crest ini.xyz  -T 14
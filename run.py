from toxic_ms import RunAnalysis
import argparse
import os

Description = """
This script should help with pre-identification of specties in mass-spectra (MS) from the database.
Disclaimer:
  1) This is only a tentative assignment. Please trust experts and specialized software!
  2) This is a home-made script, so no warranties...
"""

if __name__ == "__main__":
    print(Description)
    parser = argparse.ArgumentParser(description="Welcome to our script for assigning Mass-spectra!")
    
    parser.add_argument('-f', '--file', type=str, default='experimental.dat',
                    help="Filename with a MS data to process. Default is 'experimental.dat'.")

    parser.add_argument('-d', '--database', type=str, default=None,
                    help="Name of the folder with a MS database. Default is 'database'.")

    parser.add_argument('--width', type=float, default=1.5,
                    help="Width of the peak-cutting function in m/z units. Default is 1.5") 
    
    parser.add_argument('--CleanSpectrum', action='store_true', 
                    help="Flag to clean the baseline of the spectrum.")
    
    parser.add_argument('--NumCleanIter', type=int, default=3,
                    help="Number of cleaning iterations Default is 3.") 

    parser.add_argument('--ScaleBase', type=float, default=3.,
                    help="Scale factor for the baseline during cleaning. Default is 3.") 
    
    parser.add_argument('--OnlyPeaks', action='store_true', 
                    help="Flag to indicate that the data file contains only the peaks' positions.")
                        
    args,unknown = parser.parse_known_args() # parse the arguments

    if args.database is None:
        args.database = os.path.dirname(__file__) + "/database"
        #print(args.database)

    RunAnalysis(FileToAnalyze=args.file, DataBaseFolder=args.database,
                PerformSpectrumCleaning=args.CleanSpectrum, NumIter=args.NumCleanIter, Scale=args.ScaleBase,
                Width=args.width,OnlyPeaks=args.OnlyPeaks)


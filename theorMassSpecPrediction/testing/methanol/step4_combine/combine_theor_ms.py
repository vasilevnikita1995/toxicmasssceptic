import numpy as np

Sp = np.loadtxt("ref_qcxms.ms")

X1 = list(np.array(Sp[:,0], dtype=int))
Y1 = np.array(Sp[:,1], dtype=float)

print(np.sum(Y1))

Sp = np.loadtxt("ref_dissmd.ms")

X2 = list(np.array(Sp[:,0], dtype=int))
Y2 = np.array(Sp[:,1], dtype=float)

print(np.sum(Y2))

UniqMZ = np.sort( list(set(X1 + X2)) )
UniqInt = np.zeros(UniqMZ.shape, dtype=float)

for n,mz in enumerate(UniqMZ):
    try:
        ind1 = X1.index(mz)
    except:
        ind1 = None
    try:
        ind2 = X2.index(mz)
    except:
        ind2 = None    
        
    if (not ind1 is None) and (not ind2 is None):
        UniqInt[n] = 0.5 * (Y1[ind1] + Y2[ind2])
    elif (not ind1 is None):
        UniqInt[n] = 0.5 * Y1[ind1]
    elif (not ind2 is None):  
        UniqInt[n] = 0.5 * Y2[ind2]
    else:
        raise RuntimeError("WTF?!")  
        
UniqInt *= 100./np.sum(UniqInt)

np.savetxt("ref.ms", np.stack([UniqMZ,UniqInt], axis=-1))
        

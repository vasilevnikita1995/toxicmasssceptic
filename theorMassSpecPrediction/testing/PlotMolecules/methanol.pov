// ******************************************************
// Created by Jmol 14.6.4_2016.11.05  2016-11-10 03:30
//
// This script was generated on Thu, 31 Aug 2023 12:21:37 +0200
// ******************************************************

/****** Jmol Embedded Script **** 
# Jmol state version 14.6.4_2016.11.05  2016-11-10 03:30;

function _setWindowState() {
# preferredWidthHeight -1 -1;
# width -1;
# height -1;
  stateVersion = 1406004;
  background [x000000];
  axis1Color = "[xff0000]";
  axis2Color = "[x008000]";
  axis3Color = "[x0000ff]";
  set ambientPercent 45;
  set diffusePercent 84;
  set specular true;
  set specularPercent 22;
  set specularPower 40;
  set specularExponent 6;
  set celShading false;
  set celShadingPower 10;
  set zShadePower 3;
}

function _setFileState() {

  set allowEmbeddedScripts false;
  set appendNew true;
  set appletProxy "";
  set applySymmetryToBonds false;
  set autoBond true;
  set bondRadiusMilliAngstroms 150;
  set bondTolerance 0.47;
  set defaultLattice {0.0 0.0 0.0};
  set defaultLoadFilter "";
  set defaultLoadScript "";
  set defaultStructureDssp true;
  set defaultVDW Auto;
  set forceAutoBond false;
  #set defaultDirectory "/home/madschumacher/work/toxicmasssceptic/theorMassSpecPrediction/testing/gb_sarin/step2_crest";
  #set loadFormat "https://files.rcsb.org/download/%FILE.pdb";
  #set loadLigandFormat "https://files.rcsb.org/ligands/download/%FILE.cif";
  #set smilesUrlFormat "https://cactus.nci.nih.gov/chemical/structure/%FILE/file?format=sdf&get3d=true";
  #set nihResolverFormat "https://cactus.nci.nih.gov/chemical/structure/%FILE";
  #set pubChemFormat "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/%FILE/SDF?record_type=3d";
  #set edsUrlFormat "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file.omap";
  #set edsUrlFormatDiff "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file_diff.omap";
  #set edsUrlCutoff "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file.sfdat";
  set bondingVersion 0;
  set legacyAutoBonding false;
  set legacyHAddition false;
  set legacyJavaFloat false;
  set minBondDistance 0.07;
  set minimizationCriterion  0.001;
  set minimizationSteps  100;
  set multipleBondBananas false;
  set pdbAddHydrogens false;
  set pdbGetHeader false;
  set pdbSequential false;
  set percentVdwAtom 23;
  set smallMoleculeMaxAtoms 40000;
  set smartAromatic true;
  load /*file*/"./xtbopt.xyz";

}

function _setParameterState() {

   set defaultanglelabel "%VALUE %UNITS";
   set defaultcolorscheme "jmol";
   set defaultdistancelabel "%VALUE %UNITS";
   set defaultdrawarrowscale 0.5;
   set defaultlabelpdb "%m%r";
   set defaultlabelxyz "%a";
   set defaultlattice "{0 0 0}";
   set defaultloadfilter "";
   set defaultloadscript "";
   set defaulttorsionlabel "%VALUE %UNITS";
   set defaulttranslucent 0.5;
   set defaultvdw "Auto";
  set allowembeddedscripts true;
  set allowmoveatoms false;
  set allowrotateselected false;
  set animationmode "once";
  set appletproxy "";
  set applysymmetrytobonds false;
  set atomtypes "";
  set autobond true;
  set autofps false;
  set axes window;
  set axesmode 0;
  set axesoffset 0.0;
  set axesscale 2.0;
  set backbonesteps false;
  set bondmodeor false;
  set bondradiusmilliangstroms 150;
  set bondtolerance 0.47;
  set cartoonbaseedges false;
  set cartoonfancy false;
  set cartoonladders false;
  set cartoonrockets false;
  set chaincasesensitive false;
  set dataseparator "~~~";
  set defaultstructuredssp true;
  set delaymaximumms 0;
  set dipolescale 1.0;
  set disablepopupmenu false;
  set displaycellparameters true;
  set dotdensity 3;
  set dotscale 1;
  set dotsselectedonly false;
  set dotsurface true;
  set dragselected false;
  set drawfontsize 14.0;
  set drawhover false;
  set dsspcalculatehydrogenalways true;
  set edsurlformatdiff "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file_diff.omap";
  set ellipsoidarcs false;
  set ellipsoidarrows false;
  set ellipsoidaxes false;
  set ellipsoidaxisdiameter 0.02;
  set ellipsoidball true;
  set ellipsoiddotcount 200;
  set ellipsoiddots false;
  set ellipsoidfill false;
  set energyunits "kJ";
  set forceautobond false;
  set gestureswipefactor 1.0;
  set greyscalerendering false;
  set hbondsangleminimum 90.0;
  set hbondsbackbone false;
  set hbondsdistancemaximum 3.25;
  set hbondsrasmol true;
  set hbondssolid false;
  set helixstep 1;
  set helppath "http://chemapps.stolaf.edu/jmol/docs/index.htm";
  set hermitelevel 0;
  set hiddenlinesdashed false;
  set hidenameinpopup false;
  set hidenavigationpoint false;
  set highresolution false;
  set hoverdelay 0.5;
  set isosurfacekey false;
  set isosurfacepropertysmoothing true;
  set isosurfacepropertysmoothingpower 7;
  set justifymeasurements false;
  set loadatomdatatolerance 0.01;
  set measureallmodels false;
  set measurementlabels true;
  set meshscale 1;
  set messagestylechime false;
  set minbonddistance 0.07;
  set minimizationcriterion 0.001;
  set minimizationrefresh true;
  set minimizationsilent false;
  set minimizationsteps 100;
  set minpixelselradius 6;
  set modulationscale 1.0;
  set monitorenergy false;
  set multiplebondbananas false;
  set multiplebondradiusfactor 0.0;
  set multiplebondspacing -1.0;
  set navigationperiodic false;
  set navigationspeed 5.0;
  set nmrpredictformat "http://www.nmrdb.org/service/predictor?POST?molfile=";
  set nmrurlformat "http://www.nmrdb.org/new_predictor?POST?molfile=";
  set partialdots false;
  set particleradius 20.0;
  set pdbaddhydrogens false;
  set pdbgetheader false;
  set pdbsequential false;
  set percentvdwatom 23;
  set pickingspinrate 10;
  set pointgroupdistancetolerance 0.2;
  set pointgrouplineartolerance 8.0;
  set propertyatomnumbercolumncount 0;
  set propertyatomnumberfield 0;
  set propertycolorscheme "roygb";
  set propertydatacolumncount 0;
  set propertydatafield 0;
  set quaternionframe "p";
  set rangeselected false;
  set ribbonaspectratio 16;
  set ribbonborder false;
  set rocketbarrels false;
  set selecthetero true;
  set selecthydrogen true;
  set sheetsmoothing 1.0;
  set showhiddenselectionhalos false;
  set showhydrogens true;
  set showmeasurements true;
  set showmodulationvectors false;
  set showmultiplebonds true;
  set shownavigationpointalways false;
  set showunitcelldetails true;
  set slabbyatom false;
  set slabbymolecule false;
  set smallmoleculemaxatoms 40000;
  set smartaromatic true;
  set solventprobe false;
  set solventproberadius 1.2;
  set ssbondsbackbone false;
  set starwidth 0.05;
  set stereodegrees -5;
  set strandcountformeshribbon 7;
  set strandcountforstrands 5;
  set strutdefaultradius 0.3;
  set strutlengthmaximum 7.0;
  set strutsmultiple false;
  set strutspacing 6;
  set tracealpha true;
  set translucent true;
  set twistedsheets false;
  set usenumberlocalization true;
  set vectorscale 1.0;
  set vectorscentered false;
  set vectorsymmetry false;
  set vectortrail 0;
  set vibrationscale 0.5;
  set waitformoveto true;
  set wireframerotation false;
  set zdepth 0;
  set zoomheight false;
  set zoomlarge true;
  set zslab 50;

}

function _setModelState() {


  frank on;
  font frank 16.0 SansSerif Plain;
  select *;
  set fontScaling false;

}

function _setPerspectiveState() {
  set perspectiveModel 11;
  set scaleAngstromsPerInch 0.0;
  set perspectiveDepth true;
  set visualRange 5.0;
  set cameraDepth 0.2;
  boundbox corners {-0.02117443 0.0061255693 3.25799E-4} {2.5145974 1.711838 1.5990522} # volume = 6.914967;
  center {1.2467115 0.8589818 0.799689};
   moveto -1.0 {0 0 1 0} 100.0 0.0 0.0 {1.2467115 0.8589818 0.799689} 2.8599496 {0 0 0} 0 0 0 0.2 0.0 0.0;
  save orientation "default";
  moveto 0.0 { 831 -532 162 50.88} 100.0 0.0 0.0 {1.2467115 0.8589818 0.799689} 2.8599496 {0 0 0} 0 0 0 0.2 0.0 0.0;;
  slab 100;depth 0;
  set slabRange 0.0;
  set spinX 0; set spinY 30; set spinZ 0; set spinFps 30;  set navX 0; set navY 0; set navZ 0; set navFps 10;
}

function _setSelectionState() {
  select ({0:5});
  set hideNotSelected false;
}

function _setState() {
  initialize;
  set refreshing false;
  _setWindowState;
  _setFileState;
  _setParameterState;
  _setModelState;
  _setPerspectiveState;
  _setSelectionState;
  set refreshing true;
  set antialiasDisplay false;
  set antialiasTranslucent true;
  set antialiasImages true;
}

_setState;

**/
// Jmol perspective:
// screen width height dim: 1000 968 1000
// perspectiveDepth: true
// cameraDistance(angstroms): 4.0039287
// aperatureAngle(degrees): 71.075356
// scalePixelsPerAngstrom: 174.47862
// light source: {-0.34815532, -0.34815532, 0.87038827}
// lighting:   set ambientPercent 45;   set diffusePercent 84;   set specular true;   set specularPercent 22;   set specularPower 40;   set specularExponent 6;   set celShading false;   set celShadingPower 10;   set zShadePower 3;   set zDepth 0;   set zSlab 50;   set zShade false; 
// center: {1.2467115, 0.8589818, 0.799689}
// rotationRadius: 2.8599496
// boundboxCenter: {1.2467115, 0.8589818, 0.799689}
// translationOffset: 
// zoom: 100.0
// moveto command: moveto 1.0 { 831 -532 162 50.88} 100.0 0.0 0.0 {1.2467115 0.8589818 0.799689} 2.8599496 {0 0 0} 0 0 0 0.2 0.0 0.0;

// ******************************************************
// Declare the resolution, camera, and light sources.
// ******************************************************

// NOTE: if you plan to render at a different resolution,
// be sure to update the following two lines to maintain
// the correct aspect ratio.

#declare Width = 1000;
#declare Height = 968;
#declare minScreenDimension = 968;
#declare showAtoms = true;
#declare showBonds = true;
#declare noShadows = true;
camera{
  perspective
  angle 71.075356
  right < 1000, 0, 0>
  up < 0, -968, 0 >
  sky < 0, -1, 0 >
  location < 500.0, 484.0, 0>
  look_at < 500.0, 484.0, 1000 >
}

background { color rgb <0.0,0.0,0.0> }

light_source { <-348.15533,-348.15533, -870.38824>  rgb <0.6,0.6,0.6> }


// ***********************************************
// macros for common shapes
// ***********************************************

#default { finish {
  ambient 0.45
  diffuse 0.84
  specular 0.22
  roughness .00001
  metallic
  phong 0.9
  phong_size 120
}}

#macro check_shadow()
 #if (noShadows)
  no_shadow 
 #end
#end

#declare slabZ = 0;
#declare depthZ = 2147483647;
#declare dzSlab = 10;
#declare dzDepth = dzSlab;
#declare dzStep = 0.001;

#macro clip()
  clipped_by { box {<0,0,slabZ>,<Width,Height,depthZ>} }
#end

#macro circleCap(Z,RADIUS,R,G,B,T)
// cap for lower clip
 #local cutDiff = Z - slabZ;
 #local cutRadius2 = (RADIUS*RADIUS) - (cutDiff*cutDiff);
 #if (cutRadius2 > 0)
  #local cutRadius = sqrt(cutRadius2);
  #if (dzSlab > 0)
   #declare dzSlab = dzSlab - dzStep;
  #end
  cylinder{<X,Y,slabZ-dzSlab>,<X,Y,(slabZ+1)>,cutRadius
   pigment{rgbt<R,G,B,T>}
   translucentFinish(T)
   check_shadow()}
 #end
// cap for upper clip
 #declare cutDiff = Z - depthZ;
 #declare cutRadius2 = (RADIUS*RADIUS) - (cutDiff*cutDiff);
 #if (cutRadius2 > 0)
  #local cutRadius = sqrt(cutRadius2);
  #if (dzDepth > 0)
   #declare dzDepth = dzDepth - dzStep;
  #end
  cylinder{<X,Y,depthZ+dzDepth>,<X,Y,(depthZ-1)>,cutRadius
   pigment{rgbt<R,G,B,T>}
   translucentFinish(T)
   check_shadow()}
 #end
#end

#macro translucentFinish(T)
 #local shineFactor = T;
 #if (T <= 0.25)
  #declare shineFactor = (1.0-4*T);
 #end
 #if (T > 0.25)
  #declare shineFactor = 0;
 #end
 finish {
  ambient 0.45
  diffuse 0.84
  specular 0.22
  roughness .00001
  metallic shineFactor
  phong 0.9*shineFactor
  phong_size 120*shineFactor
}#end

#macro a(X,Y,Z,RADIUS,R,G,B,T)
 sphere{<X,Y,Z>,RADIUS
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

#macro q(XX,YY,ZZ,XY,XZ,YZ,X,Y,Z,J,R,G,B,T)
 quadric{<XX,YY,ZZ>,<XY,XZ,YZ>,<X,Y,Z>,J
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

#macro b(X1,Y1,Z1,RADIUS1,X2,Y2,Z2,RADIUS2,R,G,B,T)
 cone{<X1,Y1,Z1>,RADIUS1,<X2,Y2,Z2>,RADIUS2
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

#macro c(X1,Y1,Z1,RADIUS1,X2,Y2,Z2,RADIUS2,R,G,B,T)
 cone{<X1,Y1,Z1>,RADIUS1,<X2,Y2,Z2>,RADIUS2 open
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

a(294.0,573.0,692.0,44.0,1.0,1.0,1.0,0)
a(417.0,440.0,753.0,68.0,0.56640625,0.56640625,0.56640625,0)
a(318.0,289.0,813.0,44.0,1.0,1.0,1.0,0)
a(517.0,511.0,900.0,44.0,1.0,1.0,1.0,0)
a(563.0,361.0,573.0,60.5,1.0,0.0546875,0.0546875,0)
a(653.0,490.0,515.0,44.0,1.0,1.0,1.0,0)
b(563.0,361.0,573.0,26.171793,608.0,425.5,544.0,26.171793,1.0,0.0546875,0.0546875,0)
b(608.0,425.5,544.0,26.171793,653.0,490.0,515.0,26.171793,1.0,1.0,1.0,0)
a(563.0,361.0,573.0,26.171793,1.0,0.0546875,0.0546875,0)
a(653.0,490.0,515.0,26.171793,1.0,1.0,1.0,0)
b(318.0,289.0,813.0,26.171793,367.5,364.5,783.0,26.171793,1.0,1.0,1.0,0)
b(367.5,364.5,783.0,26.171793,417.0,440.0,753.0,26.171793,0.56640625,0.56640625,0.56640625,0)
a(318.0,289.0,813.0,26.171793,1.0,1.0,1.0,0)
a(417.0,440.0,753.0,26.171793,0.56640625,0.56640625,0.56640625,0)
b(417.0,440.0,753.0,26.171793,490.0,400.5,663.0,26.171793,0.56640625,0.56640625,0.56640625,0)
b(490.0,400.5,663.0,26.171793,563.0,361.0,573.0,26.171793,1.0,0.0546875,0.0546875,0)
a(417.0,440.0,753.0,26.171793,0.56640625,0.56640625,0.56640625,0)
a(563.0,361.0,573.0,26.171793,1.0,0.0546875,0.0546875,0)
b(417.0,440.0,753.0,26.171793,467.0,475.5,826.5,26.171793,0.56640625,0.56640625,0.56640625,0)
b(467.0,475.5,826.5,26.171793,517.0,511.0,900.0,26.171793,1.0,1.0,1.0,0)
a(417.0,440.0,753.0,26.171793,0.56640625,0.56640625,0.56640625,0)
a(517.0,511.0,900.0,26.171793,1.0,1.0,1.0,0)
b(294.0,573.0,692.0,26.171793,355.5,506.5,722.5,26.171793,1.0,1.0,1.0,0)
b(355.5,506.5,722.5,26.171793,417.0,440.0,753.0,26.171793,0.56640625,0.56640625,0.56640625,0)
a(294.0,573.0,692.0,26.171793,1.0,1.0,1.0,0)
a(417.0,440.0,753.0,26.171793,0.56640625,0.56640625,0.56640625,0)

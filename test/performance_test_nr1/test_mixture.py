#! /usr/bin/python

import sys
sys.path.append('../../src/toxic_ms')

import numpy as np
import toxicMS as tms

x0 = 0.
x1 = 200.
Npts = 1000
W = 0.2

# Step 1.1: make the background
# add 78% of N2
N2 = np.loadtxt("background/n2/ref.ms")
Xt,Yt = tms.GenerateSpectrum(MS=N2, xini=x0, xfin=x1, npts=Npts, scale=0.78, width=W)

# save the m/z (x)-axis and Y-axis
Xb = np.array(Xt)
Yb = np.array(Yt)

# add 20% of O2
O2 = np.loadtxt("background/o2/ref.ms")
Xt,Yt = tms.GenerateSpectrum(MS=O2, xini=x0, xfin=x1, npts=Npts, scale=0.20, width=W)
Yb += Yt

# add 1% of CO2
CO2 = np.loadtxt("background/co2/ref.ms")
Xt,Yt = tms.GenerateSpectrum(MS=CO2, xini=x0, xfin=x1, npts=Npts, scale=0.1, width=W)
Yb += Yt

# add 10% of C6H6
Benz = np.loadtxt("background/c6h6/ref.ms")
Xt,Yt = tms.GenerateSpectrum(MS=Benz, xini=x0, xfin=x1, npts=Npts, scale=0.1, width=W)
Yb += Yt

# add 20% of Farnesene
Benz = np.loadtxt("background/farnesene/ref.ms")
Xt,Yt = tms.GenerateSpectrum(MS=Benz, xini=x0, xfin=x1, npts=Npts, scale=0.2, width=W)
Yb += Yt

# Step 1.2: add the noise
YNoi3e = 0.01 * np.random.uniform(low=-1., high=1.0, size=Npts)

np.savetxt("plot/background.dat", np.stack([Xb, Yb+YNoi3e, Yb, YNoi3e],axis=-1))
Yb += YNoi3e

# Step 2: load all poisons :)
ModelY = [  ]
ModelY.append(Yb)

Spec = []
Spec.append(np.loadtxt("poisons/h.ms"))
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)

Spec.append(np.loadtxt("poisons/q.ms"))      
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)
 
Spec.append(np.loadtxt("poisons/t.ms"))    
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)

           
Spec.append(np.loadtxt("poisons/ga.ms"))                       
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)

Spec.append(np.loadtxt("poisons/gb.ms"))  
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)

Spec.append(np.loadtxt("poisons/vx.ms"))                       
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)

Spec.append(np.loadtxt("poisons/ve.ms"))  
Xt,Yt = tms.GenerateSpectrum(MS=Spec[-1], xini=x0, xfin=x1, npts=Npts, scale=1, width=W)
ModelY.append(Yb + Yt)

np.savetxt("plot/allspec.dat", np.stack([Xb] + ModelY, axis=-1) )

CleanY = [tms.CleanSpectrum(y, NumIter=3, Scale = 3) for y in ModelY]
np.savetxt("plot/allcleanspec.dat", np.stack([Xb] + CleanY, axis=-1) )

DKL = np.zeros( (len(Spec),len(Spec)+1), dtype=float )
DBH = np.zeros( DKL.shape, dtype=float )
DHE = np.zeros( DKL.shape, dtype=float )
NP = np.zeros( DKL.shape, dtype=int )

for i in range(0,len(Spec)):
    for j in range(0,len(ModelY)):
        Npeaks, N, dkl, dbh, dhe = tms.CompareSpectrumWithRef(Xb, CleanY[j], Spec[i], width=1.0)   

        DKL[i][j] = dkl
        DBH[i][j] = dbh
        DHE[i][j] = dhe
        NP[i][j]  = Npeaks

np.savetxt("plot/dkl.mt", DKL)
np.savetxt("plot/dbh.mt", DBH)
np.savetxt("plot/dhe.mt", DHE)
np.savetxt("plot/np.mt", NP)





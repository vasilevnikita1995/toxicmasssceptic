# Toxic Mass Sceptic (TMS)

This repository contains the proof-of-principle database and software for identifying toxic (poisonous) compounds in the mass-spectra.

## To install:
```
python3 -m venv .venv
source .venv/bin/activate    
python3 -m pip install --upgrade pip
pip install .
```

## To run:
```
python run.py
```

To run:
python run.py



#!/bin/bash


#SBATCH --partition=maxcpu   # this is the default partition.
#SBATCH --time=45:00:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.
#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like 
#SBATCH --job-name  QCxMS!
#SBATCH --output     job.out  # File to which STDOUT will be written
#SBATCH --error      job.err  # File to which STDERR will be written
#SBATCH --mail-type END                 # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user denis.tikhonov@desy.de  # Email to which notifications will be sent


PrPerTask=2



echo "Heeelllllooooo" $(nproc)
aprocs=$(nproc --all)
echo "Total number of processors on $HOSTNAME is $aprocs "
procs=$(( $aprocs / 2 ))
echo " Number of phyiscal cores on $HOSTNAME $procs "
 
# total number of task for simulation
NumSimTasks=$(echo "0.8 * $procs/$PrPerTask" | bc )
echo "NumSimTasks is $NumSimTasks "

NT=$(echo "0.8 * $procs" | bc )
NT=$(echo $NT | grep -o '^[0-9]\+')

export MKL_NUM_THREADS=$NT
export OMP_NUM_THREADS=$NT
export OMP_STACKSIZE=10G

qcxms -i ini.xyz
qcxms -i ini.xyz


pqcxms -j $NumSimTasks -t $PrPerTask

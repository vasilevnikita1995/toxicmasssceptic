# Theoretical calculation of mass-spectra

Here, we have the calculation of theoretical mass-spectra (MS) of various compounds.
The 'testing' folder contains species with known experimental spectra, folder 'prediction' contains predictions of unavailable experimenal MS. The 'workflow_scheme' contains the graphical representation of this procedure.

Each of these two folders ('testing' and 'prediction') contain folders with substances, where the workflow is separated into subfolders named as
  - step1_xtb
  - step2_crest
  - step3-1_qcxms
  - step3-2_dissmd
  - step4_combine

They contain the inputs and outputs of the corresponding jobs.

## Step 1: xTB
In the folder 'step1_xtb' there is a preliminary optimization of the molecular structure with xTB software. The input file is either an XYZ file 'ini.xyz', or an MDL Molfile, e.g., drawn with Jmol software (jmol.mol). The output is either an xTB output 'xtbopt.xyz', or a similar molfile ('xtbopt.mol').

## Step 2: CREST
In the 'step2_crest', the conformational search with the CREST software is performed. The input file for this job is 'ini.xyz', which was obtained from the previous step. Either it is the 'xtbopt.xyz' file, or the structure extracted from the 'xtbopt.mol' ('fin.xyz' in 'step1_xtb'). The following files are contained in the folder:
 - ini.xyz -- the initial guess XYZ structure of the molecule,
 - sbatch_crest.sh -- SLURM submission script for the CREST job,
 - job.out -- (std) output of the CREST,
 - crest_conformers.xyz -- XYZ trajectory file of all found conformers,
 - crest.energies -- file with conformers' energies,
 - crest_best.xyz -- the most stable conformer.
 
File 'crest_best.xyz' was used as the input for the next step.

## Step 3: Theoretical calculation of the mass-spectra
The theoretical calculation of the mass-spectra is done in both cases using 'crest_best.xyz' file from previous step, that is renamed into 'ini.xyz'. We use two softwares for performing such calculations. We do not provide the trajectories here, due to them taking large volume of space.

### Step 3-1: QCxMS
First, we run the calculation of the MS using QCxMS software in the electron impact (EI) ionization regime. The input with settings for the job is the 'qcxms.in', which is taken from the test example of the QCxMS. The SLURM submission script is 'sbatch.sh'. The output of the job is given in 'job.out' file. The calculation of the MS using 'getres' and 'plotms' additional programms, is done by 'calculate_ms_qcxms.sh' Bash script. It produces the 'getres'+'plotms' outputs 'result.csv', 'result.jdx', and 'ms_spec.log', which are turned into integer MS 'ref.ms'.
         
### Step 3-2: DissMD
Second, we run the calculation of the MS using DissMD software from the PyRAMD project. The ionization is in the laser ionization regime, which is adjusted in the case of test molecule, sarine, in trial subfolders. 

The required files for the job submission are stored in the 'ini' subfolder, containing four files.
 - 'batch_dissmd.sh' is the SLURM submission script with all the command-line settings of the calculation.
 - 'ini.xyz' is the initial file ('crest_best.xyz' from step 2).
 - 'pulse.def' is the excitation settings file.
 - 'submit_jobs.sh' is the script, that initialized multiple sets of jobs for parallel calculations on multiple nodes.

The results of the calculations are collected using 'calculate_ms.sh' script, that produces four files: 'md.log', 'ms.dat', 'ms_process.log', and 'ref.ms'. The last of them is the desired mass-spectrum. 'md.log' contains all the final charged fragments found in the simulations. 'ms.dat' is another representation of the MS, in the form of distribution. 'ms_process.log' is the log of the processing.

## Step 4: Combination of the theoretical mass-spectra
In this step, we combine the MS ('ref.ms') from Steps 3-1 and 3-2 using script 'combine_theor_ms.py'. We rename those two files as 'ref_qcxms.ms' and 'ref_dissmd.ms', respectively, and obtain a new theoretical file 'ref.ms'. In the case of the test molecules, figures are also made with Gnuplot (via script 'plot.gnp') and experimental data 'ref_exp.ms'. 







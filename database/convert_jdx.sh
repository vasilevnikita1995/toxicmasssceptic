#! /bin/bash

inipath=$(pwd)

for d in $(ls | grep -i 'agent'); do
    echo $d
    cd $inipath/$d
    
    for dd in $(ls); do
        cd $inipath/$d/$dd
        F2have=$(ls | grep '\.jdx$' )
        echo $F2have
######################################
python <<-EOF

import sys
sys.path.append('../../../../')
sys.path.append('../../../')
sys.path.append('../../')
sys.path.append('../')
import toxicMS as tms         
        
tms.ConvertJDXToDatFile("$F2have")

EOF
######################################
    cd ../
    done
    
done

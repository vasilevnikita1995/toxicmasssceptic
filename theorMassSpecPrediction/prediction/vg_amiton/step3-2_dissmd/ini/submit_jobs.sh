#! /bin/bash
 
NJobs=5
 
IniDir=$(pwd)
 
Last=$(ls ../ | grep 'set[0-9]\+'  | sed 's/set//g' | sort -n | tail -1)
if [ -z "$Last" ]; then
Last=0
fi
 
New=$(echo "$Last + 1" | bc)
echo "$Last -> $New"
Last=$(echo "$New + $NJobs" | bc)
 
for (( i=$New; i<=$Last; i++ )); do
   cd $IniDir
   dname=$(printf "set%i" $i)
   echo $dname
   mkdir ../$dname
   cp batch_dissmd.sh  ini.xyz  pulse.def  ../$dname
   cd ../$dname
   sbatch batch_dissmd.sh
   cd $IniDir
done

#! /bin/bash

inipath=$(pwd)

for d in $(ls | grep -i '^[a-zA-Z][a-zA-Z]\?[0-9]'); do
    echo $d
    cd $inipath/$d
    
    F2have=$(ls | grep '\.jdx$' )
    echo $F2have
######################################
python <<-EOF

import sys
sys.path.append('../../../../')
sys.path.append('../../../')
sys.path.append('../../')
sys.path.append('../')
import toxicMS as tms         
        
tms.ConvertJDXToDatFile("$F2have")

EOF
######################################
    
done

cd $inipath/farnesene
######################################
python <<-EOF

import sys
sys.path.append('../../../../')
sys.path.append('../../../')
sys.path.append('../../')
sys.path.append('../')
import toxicMS as tms         
        
tms.ConvertJDXToDatFile("502-61-4-Mass.jdx")

EOF
######################################

#! /bin/bash


rm -r -f spectra
mkdir spectra

python3 <<-EOF
import sys
sys.path.append('../../src/toxic_ms')

import numpy as np
import toxicMS as tms


db = tms.LoadDataBase(DataBaseFolder="../../database")
bg = dict()

for mol in ["c6h6",  "co2",   "farnesene",  "n2",  "o2"]:
    bg.update({mol: np.loadtxt("background/"+mol+"/ref.ms")})

Ntrials = 1000

x0 = 0
x1 = 300
npts = 1000

PossibleSubstances = list(db.keys())

my_generator = np.random.default_rng()

outf = open("save_answers.dat", "w")
outfd = open("save_dirty_answers.dat", "w")

for t in range(0,Ntrials):

    sub = my_generator.choice(PossibleSubstances)
    header = 'sub: %s' % (sub)
    amount = my_generator.uniform(low=0.5, high=1.5)
    header += " ; a = %f ; " % (amount)
    width = my_generator.uniform(low=0.05, high=0.5)
    header += "w = %f; " % (width)    
    X,Y = tms.GenerateSpectrum(MS=db[sub], xini=x0, xfin=x1, npts=npts, scale=amount, width=width)
    
    SLvl = np.mean(db[sub][:,1]) # signal level
    SNratio = my_generator.uniform(low=10, high=1000)
    
    Ynoise = (SLvl/SNratio) * my_generator.uniform(low=-1, high=1,size=X.shape[0])
    
    Y += Ynoise
    for key in bg.keys():
        abg = my_generator.uniform(low=0.1, high=0.5)
        header += "a(%s) = %f;" % (key, abg)
        Xt,Yt = tms.GenerateSpectrum(MS=bg[key], xini=x0, xfin=x1, npts=npts, scale=abg, width=width)
        Y += Yt
        
    fname = "spectra/trial_%05i.sp" % (t)    
    np.savetxt(fname, np.stack([X,Y],axis=-1), header=header)
    
    bkl,bbh,bhe = tms.RunAnalysis(FileToAnalyze=fname, DataBaseFolder="../../database",
                                      PerformSpectrumCleaning=True, NumIter=3, Scale=3,
                                      Width=1.0)
                                      
    outf.write(str(int(bkl == sub))+"   "+str(int(bbh == sub))+"   "+str(int(bhe == sub))+"   "+sub+"\n")
    outf.flush()

    bkl,bbh,bhe = tms.RunAnalysis(FileToAnalyze=fname, DataBaseFolder="../../database",
                                      PerformSpectrumCleaning=False, NumIter=3, Scale=3,
                                      Width=1.0)

    outfd.write(str(int(bkl == sub))+"   "+str(int(bbh == sub))+"   "+str(int(bhe == sub))+"   "+sub+"\n")
    outfd.flush()

outf.close()
outfd.close()


outf = open("final.stats", "w")
outf.write("*********** FINAL STATS *************\n\n")
    
def PrintOutStats(data,outf):    
    N = len(data)
    p = np.sum(data[:,0])/N
    err = np.sqrt(p*(1-p)/N)
    outf.write(" PKL = %.2f +/- %.2f %% \n" % (p*100, err*100))

    p = np.sum(data[:,1])/N
    err = np.sqrt(p*(1-p)/N) 
    outf.write(" PBh = %.2f +/- %.2f %% \n" % (p*100, err*100))

    p = np.sum(data[:,2])/N
    err = np.sqrt(p*(1-p)/N)
    outf.write(" PHe = %.2f +/- %.2f %% \n" % (p*100, err*100))

    outf.write(" Pearson's coefficients:\n")
    outf.write("   rho(KL,Bh) = %f \n" % (np.corrcoef(data[:,0],data[:,1])[0][1]))
    outf.write("   rho(KL,He) = %f \n" % (np.corrcoef(data[:,0],data[:,2])[0][1]))    
    outf.write("   rho(Bh,He) = %f \n" % (np.corrcoef(data[:,1],data[:,2])[0][1]))    

data = np.loadtxt("save_answers.dat", usecols=(0,1,2), dtype=int)
outf.write("Cleaned stats:\n")
PrintOutStats(data,outf)

data = np.loadtxt("save_dirty_answers.dat", usecols=(0,1,2), dtype=int)
outf.write("Uncleaned stats:\n")
PrintOutStats(data,outf)

EOF

